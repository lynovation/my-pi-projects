var fs = require('fs');
//var postRadioHTML = fs.readFileSync('/var/lib/cloud9/iCTR/index.html');
//var exec = require("child_process").exec;
var querystring = require("querystring");

//var comm = require('/var/lib/cloud9/iCTR/comm');

//var tmr = ""; // used for keepAlive function

function start(response, postData) {
    console.log("Request handler 'start' was called.");
    // -> BeagleBone: var data = fs.readFileSync('/var/lib/cloud9/iCTR/index.html','UTF-8');
    var data = fs.readFileSync('../index.html','UTF-8');
    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(data);
    response.end(); 
}


function upload(response, postData) {
    console.log("Request handler 'upload' was called.");
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("You've sent the text: \n\r"+
      querystring.parse(postData).text);
    response.end();
}

exports.start = start;
exports.upload = upload;
