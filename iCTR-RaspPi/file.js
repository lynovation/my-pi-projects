var fs = require('fs'); //file system
var querystring = require("querystring");

//set arrays up as 21 bands of 12 buttons each
var freqArray = new Array(21*12);
var modeArray = new Array(21*12);
var filterArray = new Array(21*12);

var path = require('path');
var filePath = path.join(__dirname, + '') + '/';

/*
//test VFO-A btn 1, band 6
freqArray[1] = '100.3';
modeArray[1] = '6';
filterArray[1] = '4';
*/

exports.loadMemArrays = function() {
    //load mem button arrays from server side text file   
    fs.readFile(filePath + 'freqTable.txt', function(err, data) {
        var i=1;
        if(err) {
            //reset all arrays if no freq file
            for(i in array) {
                freqArray[i] = null;
                modeArray[i] = null;
                filterArray[i] = null;
            }
            return;
        }
        var array = data.toString().split(",");
        for(i in array) {
            freqArray[i] = array[i];
        }
    });
    fs.readFile(filePath + 'modeTable.txt', function(err, data) {
        if(err) {
            return; //throw err;
        }
        var array = data.toString().split(",");
        for(var i in array) {
            modeArray[i] = array[i];
        }
    });
    fs.readFile(filePath + 'filterTable.txt', function(err, data) {
        if(err) {
            return; //throw err;
        }
        var array = data.toString().split(",");
        for(var i in array) {
            filterArray[i] = array[i];
        }
    });

};

exports.getMemArray = function(btn, band) {
    //get freq/mode/filter from band & btn
    band = band - 1;
    btn = btn -1;
    
    var memPtr = (band * 21) + btn;
    var array = new Array(2);
    array[0] = freqArray[memPtr];
    array[1] = modeArray[memPtr];
    array[2] = filterArray[memPtr];
    
    return array.join();
 
};

exports.setMemArray = function(btn, band, freq, mode, filter) {
    //set freq/mode/filter arrays based on band & button selected
    band= band - 1;
    btn = btn - 1;
    //freq = freq * 1;
    mode= mode * 1;
    filter = filter * 1;
    
    var memPtr = (band * 21) + btn;
    
    //console.log('file:setMemArray: btn=' + btn + ' band=' + band + ' freq=' + freq + ' mode=' + mode + ' filter=' + filter + ' memPtr=' + memPtr);
    
    if (parseFloat(freq)>0) {
        freqArray[memPtr] = freq;
    } else {
        freqArray[memPtr] = "";
    }
    modeArray[memPtr] = mode;
    filterArray[memPtr] = filter;
 
};

exports.saveMemArrays = function() {
    //save mem button arrays to server side text file
    var fr = freqArray.join();
    var md = modeArray.join();
    var fl = filterArray.join();
    
    fs.writeFile(filePath + 'freqTable.txt', fr, function(err) {
        if(err) {
            console.log(err);
        } else {
            //console.log("The freqTable file was saved!");
        }
    });
    
    fs.writeFile(filePath + 'modeTable.txt', md, function(err) {
        if(err) {
            console.log(err);
        } else {
            //console.log("The modeTable file was saved!");
        }
    });
    
    fs.writeFile(filePath + 'filterTable.txt', fl, function(err) {
        if(err) {
            console.log(err);
        } else {
            //console.log("The filterTable file was saved!");
        }
    });


};


exports.getBtnFreqs = function(pathname) {
    //return with all freqs assigned to band
    var cmd = querystring.parse(pathname.substring(pathname.lastIndexOf("/") + 1));
    var array = new Array(12);
    //console.log('cmd.band= ' + cmd.band);
    var startPtr = (cmd.band-1) * 21;
    var ptr = 0;
    for (var i = startPtr; i< startPtr + array.length; i++) {
        array[ptr] = freqArray[i];
        ptr++;
    }
    return array.join(); // converts array to comma separated string
};
