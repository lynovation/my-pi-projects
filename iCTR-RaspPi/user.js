// Client side scripts to control radio

///////////////////////////////////////////////////////////////////////////////////////////////////
// CUSTOM HTML SCRIPT FOR iCTR
// The following is from the http://www.w3schools.com/dom/dom_httprequest.asp websit on xml usage

//Global variables for user.js
var dispBand = '';
var dispVFO = '';
var dispFreq = '';
var dispMode = '';
var dispFilter = '';

function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	document.cookie = name + "=" + value + expires + "; path=/";
    //console.log("Save cookie: " + name + "=" + value + expires + "; path=/");
}

function readCookie(name) {
    var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
		}
        //console.log("Cookie " + i + ": " + c);
        if (c.indexOf(nameEQ) === 0) 
          return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function resetCookies() {
	//resets all cookies to default
    var ok = confirm('Are you sure you want reset band settings back to default?');
    if (ok===false) {
        return null;
    }
    var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
		}
        //this line deletes all cookies
        document.cookie = c + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;'; 
    }
    //now save defaults
    document.getElementsByName('lstBand')[0].value = 1;
      saveBand('False');
    document.getElementsByName('btnVFO')[0].value ='VFO-B';
      switchVFO('False');
    document.getElementsByName('txtFreq')[0].value ='10';  
    document.getElementsByName('lstMode')[0].value = 1;
    document.getElementsByName('lstFilter')[0].value = 1;
    setFreqModeFilter(0);
    return null;	

}

function bandVal(){
     return document.getElementsByName('lstBand')[0].value;
}
function vfoLbl(){
    return document.getElementsByName('btnVFO')[0].value;
}
function freqVal(){
    return parseFloat(document.getElementsByName('txtFreq')[0].value);
}
function modeVal(){
    return document.getElementsByName('lstMode')[0].value;
}
function filterVal(){
    return document.getElementsByName('lstFilter')[0].value;
}

function setFreqModeFilter(freqOffset) {
    dispBand = bandVal();
    dispVFO = vfoLbl();
    dispFreq = freqVal();
    dispMode = modeVal();
    dispFilter = filterVal();

    var newFreq = dispFreq + (freqOffset / 1000000);
    var freq = Math.round(dispFreq * 1000000);
    
    //save cookies for selected vfo & band
    if (newFreq > 0) {
        createCookie('vfo', dispVFO, 365);
        createCookie(dispBand + '_' + dispVFO + '_freq', newFreq, 365);
        createCookie(dispBand + '_' + dispVFO + '_mode', dispMode, 365);
        createCookie(dispBand + '_' + dispVFO + '_filter', dispFilter, 365);
        //send to server
        loadXMLDoc('../freq=' + (freq + freqOffset) + '&mode=' + dispMode + '&filter=' + dispFilter);    
    }
}

function saveModel() {
    var modelRaw = document.getElementsByName('lstModel')[0].value;  
    createCookie('model', modelRaw, 365);
}

function saveBand(update) {
    dispBand = bandVal();  
    createCookie('band', dispBand, 365);
    if (update != 'False') {
        readMemButton(0); // set last freq used on this band
    }
    loadMemButtonLbls();
}

function bandUp() {
    //inc band
    dispBand = bandVal(); 
    //console.log('Up=' + dispBand);
    if ( dispBand >1) {
        dispBand--;
        document.getElementsByName('lstBand')[0].value = dispBand;
        saveBand('true');
    }
    loadMemButtonLbls(); // reload button labels
}

function bandDown() {
    //dec band
    var dispBand = bandVal();   
    //console.log('Down = ' + dispBand);
    if (dispBand < document.getElementsByName('lstBand')[0].length) {
        dispBand++;
        document.getElementsByName('lstBand')[0].value = dispBand;
        saveBand('true');
    }
    loadMemButtonLbls(); // reload button labels
}

function switchVFO(update) {
    dispBand = bandVal(); 
    dispVFO = vfoLbl();
    //console.log('VFO Setting= ' + vfoLbl);
    if (dispVFO.search('-A')>-1) {
        //console.log(dispVFO + '---> B');
        dispVFO = 'VFO-B';
        document.getElementsByName('btnVFO')[0].style.background = 'gray';
    } else {
        //console.log(vfoLbl + '---> A');
        dispVFO = 'VFO-A';
        document.getElementsByName('btnVFO')[0].style.background = 'green';
    }	 
    document.getElementsByName('btnVFO')[0].value = dispVFO;
    document.getElementsByName('txtFreq')[0].style.background = document.getElementsByName('btnVFO')[0].style.background;
    dispFreq = readCookie(dispBand + '_' + dispVFO + '_freq');
    if (dispFreq > 0) {
        document.getElementsByName('txtFreq')[0].value = calcMargin(dispFreq) + dispFreq;
        document.getElementsByName('lstMode')[0].value = readCookie(dispBand + '_' + dispVFO + '_mode');
        document.getElementsByName('lstFilter')[0].value = readCookie(dispBand + '_' + dispVFO + '_filter');
        setFreqModeFilter(0);
    }  
    setAB();
    if (update != 'False') {
        loadMemButtonLbls();
    }
}

function copyAB() {
    dispBand = bandVal();
    dispVFO = vfoLbl();
    dispMode = modeVal();
    dispFilter = filterVal();
    dispFreq = readCookie(dispBand + '_' + dispVFO + '_freq');
    if (dispFreq>0) {
        if (dispVFO.search('-B')>-1) {
            createCookie(dispBand + '_' + 'VFO-A_freq', dispFreq, 365);
            createCookie(dispBand + '_' + 'VFO-A_mode', dispMode, 365);
            createCookie(dispBand + '_' + 'VFO-A_filter', dispFilter, 365);
        } else {
            createCookie(dispBand + '_' + 'VFO-B_freq', dispFreq, 365);
            createCookie(dispBand + '_' + 'VFO-B_mode', dispMode, 365);
            createCookie(dispBand + '_' + 'VFO-B_filter', dispFilter, 365);
        }
    }
}

function setAB() {
    //set btnAB value & color based on btnVFO
    var v = vfoLbl();
    //console.log(v);
    var abLbl = "B=A";
    if (v.search('-A')>-1) {
        abLbl = "B=A";
        document.getElementsByName('btnAB')[0].style.background = 'gray';
    } else {
        abLbl = "A=B";
        document.getElementsByName('btnAB')[0].style.background = 'green';
    }	 
    document.getElementsByName('btnAB')[0].value = abLbl;
}


//Send command to server and process reply

function loadXMLDoc(data2Send) {
	//console.log('Raw Data --> ' + data2Send);
      
    if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  
	//decide which control to update
	xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState===4 && xmlhttp.status===200) {
            var v = vfoLbl();
            var resp = xmlhttp.responseText;
            //console.log(resp);
            if (resp.search("freq=") > -1) {
				var f= parseFloat(resp.substring(5,20)) / 1000000;
                document.getElementsByName("txtFreq")[0].value= calcMargin(f) + f; 
            } else if (resp.search("vol=") > -1) {
                var volRaw = resp.substring(4, 7);
                document.getElementsByName("txtVol")[0].value= volRaw;
                createCookie('vol', volRaw, 365);
            } else if (resp.search("sq=") > -1) {
                var sqRaw = resp.substring(3, 6);
                document.getElementsByName("txtSq")[0].value=sqRaw;
                createCookie('sq', sqRaw, 365);
            } else if (resp.search('state=') > -1) {
                if (resp.substring(6,9)==='off') {
                    //initialize radio
                    setFreqModeFilter(0);
                }
            } else if (resp.search('loadBtn') > -1) {
                //reply to 'btn=' command - returns with freqs for VFO-A and VFO-B
                var array = resp.split(',');
                for (var i =1;i<array.length ; i++) {
                    if ((v.search('-A') > -1) && (i<7)) {
                        if (array[i] === '') {
                            array[i] = '   ' + i + '   ';
                        }
                        document.getElementsByName('memBtn' + i)[0].value = array[i];
                        document.getElementsByName('memBtn' + i)[0].style.background = 'green';
                    } else if ((v.search('-B') > -1) && (i>6)) {
                        //offset i for VFO-B data
                        if (array[i] === '') {
                            array[i] = '   ' + (i-6) + '   ';
                        }
                        document.getElementsByName('memBtn' + (i - 6))[0].value = array[i];
                        document.getElementsByName('memBtn' + (i - 6))[0].style.background = 'gray';
                    }
                }
                //update radio to new freq
                readMemButton(0);
            } else if (resp.search('getBtn') > -1) {
                //reply to 'btnGet=' command - with stored freq for selected band & button
                //console.log('user: getBtn: ' + resp);
                var temp = resp.split(',');
                temp[1] = parseFloat(temp[1]); // strip off label
                if (temp[1] > 0) {
                    document.getElementsByName('txtFreq')[0].value = calcMargin(temp[1]) + temp[1];
                    document.getElementsByName('lstMode')[0].value = temp[2];
                    document.getElementsByName('lstFilter')[0].value = temp[3];
                    //console.log(temp[0] + ' Freq=' + temp[1] + ' Mode=' + temp[2] + ' Filter=' + temp[3]); 
                    setFreqModeFilter(0);
                }
            } else if (resp.search('setBtn') > -1) {
                //reply to set the specified button in memory
                //console.log('router: setBtn reply = ' + resp);
                loadMemButtonLbls(); // reload button labels
                
            // add additional decoding here
            //else if {
            
            //}
            }
        }
    };
  
	xmlhttp.open("GET",data2Send,true);
    xmlhttp.send();
}

function calcMargin(f) {
    //caluculate the spacing for txtFreq
    var mrg = "";
    if (f<1) {
        mrg = "     ";
    } else if (f<10) {
        mrg = "     ";
    } else if (f<100) {
        mrg = "   ";
    } else if (f<1000) {
        mrg  = " ";
    }
    return mrg;
}

function readMemButton(btn) {
    //read back memory button from cookie if it exists
    // use band as index
    dispBand = bandVal();
    dispVFO = vfoLbl();
    dispFreq = "";
    dispMode = "";
    dispFilter = "";
    var btnFreq = "";

    if (btn === 0) {
        //read base data
        dispFreq = readCookie(dispBand + '_' + dispVFO + '_freq');
        if (dispFreq !== null) {
            document.getElementsByName("txtFreq")[0].value= calcMargin(dispFreq) + dispFreq;
        } else {
            return null;
        }
        dispMode = readCookie(dispBand + '_' + dispVFO + '_mode');
        if (dispMode !== null) {
            document.getElementsByName('lstMode')[0].value =  dispMode;
        } else {
            return null;
        }   
        dispFilter = readCookie(dispBand + '_' + dispVFO +  '_filter');
        if (dispFilter !== null) {
            document.getElementsByName('lstFilter')[0].value =  dispFilter;
        } else {
            return null;
        }
        //if we make it to here, update the radio
        setFreqModeFilter(0);        
    } else if (document.getElementsByName('memBtn' + btn)[0].style.background !== 'red') {
        //load the freq from the button, if freq is same as txtFreq, toggle VFO
        btnFreq = document.getElementsByName('memBtn' + btn)[0].value;
        /*
        if (parseFloat(document.getElementsByName('txtFreq')[0].value) === parseFloat(document.getElementsByName('memBtn' +  btn)[0].value)) {
            //console.log('Toggling VFO');
            switchVFO('true');
            //reload pointer to new VFO
            dispVFO = vfoLbl();
        }
        */
        //setup freq/mode/filter
        
        dispFreq = freqVal();
        dispMode = modeVal();
        dispFilter = filterVal();
    
        //send reqest to server for freq
        if (dispVFO.search('-B') > -1) {
            btn = btn + 6;
        }
        btn = btn * 1; // force to #
        console.log('user: getBtn req: ../getBtn=' + btn + '&band=' + dispBand);
        
        loadXMLDoc('../getBtn=' + btn + '&band=' + dispBand);
        
    } else {
        //save the current txtFreq & label to the selected button
        dispFreq = freqVal();
        dispMode = modeVal();
        dispFilter = filterVal();
        btnFreq = document.getElementsByName('memBtn' + btn)[0].value;
    
        if (parseFloat(btnFreq) === parseFloat(dispFreq)) {
            //zero out freq on memBtn
            dispFreq = '0';
        }
        //send request to server for freq
        if (dispVFO.search('-B') > -1) {
            btn = btn + 6;
        }
        //console.log('User: Save Button: ../setBtn=' + btn + '&band=' + dispBand + '&freq=' + dispFreq + '&mode=' + dispMode  + '&filter=' + dispFilter);
        if (dispFreq !== '0') {
            var label=prompt('Enter a description for ' + dispFreq + ': ','');
            if (label!==null && label!=="") {
                dispFreq = dispFreq + ': ' + label;
            }
        }
        loadXMLDoc('../setBtn=' + btn + '&band=' + dispBand + '&freq=' + dispFreq + '&mode=' + dispMode  + '&filter=' + dispFilter);
    }
}


function loadMemButtonLbls() {
    dispBand = bandVal();
 
    loadXMLDoc('../loadBtn=0&band=' + dispBand); //replies with 'loadBtn=,' + freqArray[] for selected band
}

function toggleMemButtons() {
    //turn mem buttons red to set, back to normal color if they were red
    dispVFO = vfoLbl();
    
    for (var i=1; i<=6; i++) {
        if (document.getElementsByName('memBtn' + i)[0].style.background == 'red') {
            if (dispVFO.search('-A') > -1) {
                document.getElementsByName('memBtn' + i)[0].style.background = 'green';
            } else {
                document.getElementsByName('memBtn' + i)[0].style.background = 'gray';
            }
        } else {
            document.getElementsByName('memBtn' + i)[0].style.background = 'red'; // it's read to accept new value
        }
    }
}


// run this code after everything is loaded up

console.log("Loading cookies...");    
if (document.cookie !== null) {
//parse out settings

    var volRaw = readCookie('vol');
    if (volRaw !== null) {
        document.getElementsByName('txtVol')[0].value = volRaw;
    } else {
        document.getElementsByName('txtVol')[0].value = 60;
    }
    var sqRaw = readCookie('sq');
    if (sqRaw !== null) {
        //console.log(sqRaw);
        document.getElementsByName('txtSq')[0].value = sqRaw;
    } else {    
        document.getElementsByName('txtSq')[0].value = 100;
    }
    var modelRaw = readCookie('model');
    if (modelRaw !== null) {
        //console.log('Model=' + modelRaw);
        document.getElementsByName('lstModel')[0].value = modelRaw;
    } else {
        document.getElementsByName('lstModel')[0].value = 1;
    }
    dispBand = readCookie('band');
    if (dispBand !== null) {
        //console.log(bandVal);
        document.getElementsByName('lstBand')[0].value = dispBand;
    } else {
        document.getElementsByName('lstBand')[0].value = 1;
    }
    var dispVFO = readCookie('vfo');
    if (dispVFO !== null) {
        //console.log("VFO= " + dispVFO);
        //flip vfo so switchVFO can flip it back & set colors
        if (dispVFO.search('-B') > -1) {
            document.getElementsByName('btnVFO')[0].value = 'VFO-A';
        } else {
            document.getElementsByName('btnVFO')[0].value = 'VFO-B';
        }
        switchVFO('true');
    } else {
        document.getElementsByName('btnVFO')[0].value = 'VFO-A';
    }
    setAB(); //set A/B btn 

    dispFreq = readCookie(dispBand + '_' + dispVFO + '_freq');
    //console.log(dispBand + ' ' + dispVFO + ' ' + dispFreq);
    if (dispFreq !== null) {
        document.getElementsByName("txtFreq")[0].value= calcMargin(dispFreq) + dispFreq;
    } else {
        document.getElementsByName("txtFreq")[0].value= calcMargin('10') + '10';
    }
    dispMode = readCookie(dispBand + '_' + dispVFO + '_mode');
    if (dispMode !== null) {
        document.getElementsByName('lstMode')[0].value = dispMode;
    }
    var dispFilter = readCookie(dispBand + '_' + dispVFO + '_filter');
    if (dispFilter !== null) {
        document.getElementsByName('lstFilter')[0].value = dispFilter;
    } else {
        document.getElementsByName('lstFilter')[0].value = 1;
    }
}
    

//load memButtons array w/freq/mode/filter
loadMemButtonLbls();
    

// end of code


