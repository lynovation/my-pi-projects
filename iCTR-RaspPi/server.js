/////////////////////////////////////////////////

// assign listening port number here
// append this # to the end of the BeagleBone's URL address - example: 192.168.0.14:4256

var portNum = 4256;

/////////////////////////////////////////////////

var http= require("http");
var url = require('url');

function start(route, handle) {
  function onRequest(request, response) {
    var postData = "";
    var pathname = url.parse(request.url).pathname;

    request.setEncoding("utf8");

    request.addListener("data", function(postDataChunk) {
      postData += postDataChunk;
      console.log("Received POST data chunk '"+
      postDataChunk + "'.");
    });

    request.addListener("end", function() {
      route(handle, pathname, response, postData);
    });

  }

  http.createServer(onRequest).listen(portNum);
  console.log("Server has started.");
}

exports.start = start;
