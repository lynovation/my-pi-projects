var url = require('url');
var querystring = require("querystring");
var fs = require('fs');
var path = require('path');
var filePath = path.join(__dirname, + '') + '/';

var comm = require(filePath + 'comm');
var file = require(filePath + 'file');

var cmd = ""; // used to hold url commands
var tmr = ""; // used for keepAlive function

function route(handle, pathname, response, postData) {
  //console.log('File= ' + pathname);
  if (typeof handle[pathname] === 'function') {
    //handle controls
    handle[pathname](response, postData);  
    //console.log("function called --> " + pathname);
  }    
  else if (pathname.search('/iCTR') > -1)  {
       //serve graphic files
        pathname = './iCTR' + pathname;
        if (pathname.search('.gif') > -1) {
            contentType = 'image/gif';
        } else if (pathname.search('.jpg') > -1 ){
            contentType = 'image/jpeg';
        } else if (pathname.search('.png') > -1 ){
            contentType = 'image/png';
        }        
        
        fs.readFile(pathname, 'binary', function(error, file) {
            if(error) {
                response.writeHead(500, {"Content-Type": "text/plain"});
                response.write(error + "\n");
                response.end();
            } else {
                response.writeHead(200, {"Content-Type": contentType});
                response.write(file, "binary");
                response.end();
            }
        });
  } else if ((pathname.search('.js') > -1) || (pathname.search('.htm') > -1)) {
        //server js or html files
        pathname = './iCTR' + pathname;
        var contentType = "";
        if (pathname.search('.js') > -1) {
            contentType = 'text/javascript';
        } else {
            contentType = 'text/html';
        }
        //console.log('Path= ' + pathname);
        fs.readFile(pathname, function(error, file) {
            if(error) {
                response.writeHead(500, {"Content-Type": 'text/plain'});
                response.write(error + "\n");
                response.end();
            } else {
                response.writeHead(200, {"Content-Type": contentType});
                response.write(file, 'utf-8');
                response.end();
            }
        });
  } else if (pathname.search('/state=') > -1) {
        //toggle on-line/off-line state
        var state = pathname.substring(7, 10);
          //console.log(state);
        if (state == 'on') {
               comm.initRadio();
                //start periodic poll timer to keep radio alive
                if (tmr === "") {
                    //do nothing
                } else {
                    //clear the ping timer
                    clearInterval(tmr);
                    tmr="";
                }
                tmr = setInterval(function(){
                    comm.ping();
                    }, 10000);
                //console.log("...Set ping"); 
        } else {
            //console.log("Radio Off was called.");
            comm.radioOff();
            clearInterval(tmr);
            tmr="";           
        }
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("status=" + state);
        response.end(); 
  } else if (pathname.search('/freq=')> - 1) {
        //parse out freq, mode, filter key:value pairs
        cmd = querystring.parse(pathname.substring(pathname.lastIndexOf("/") + 1));
        
        // old - freq = pathname.substring(6, 20); 
        //fix formats
        cmd.freq= comm.formatFreq2Radio(cmd.freq);
        if (cmd.mode.length < 2) {
            cmd.mode = '0' + cmd.mode;
        }
        if (cmd.filter.length < 2) {
            cmd.filter = '0' + cmd.filter;
        }
        //send to radio
        comm.setFreq(cmd.freq, cmd.mode, cmd.filter);
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("freq=" + cmd.freq);
        response.end(); 

  } else if (pathname.search('/vol=')> -1) {
        //console.log("Vol pressed");
        //get vol key/pair
        cmd = querystring.parse(pathname.substring(pathname.lastIndexOf("/") + 1));
        
        if (cmd.vol < 0) {
            cmd.vol=0;
        } else if (cmd.vol > 255) {
            cmd.vol = 255;
        } 
        //console.log("Set Vol= " + cmd.vol);
        comm.setVol(cmd.vol);
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("vol=" + cmd.vol);
        response.end(); 
  } else if (pathname.search('/sq=')> - 1) {
        //console.log("Sq pressed");
        cmd = querystring.parse(pathname.substring(pathname.lastIndexOf("/") + 1));
        if (cmd.sq < 0) {
            cmd.sq=0;
        } else if (cmd.sq > 255) {
            cmd.sq = 255;
        } 
        //console.log("Set Sq= " + cmd.sq);
        comm.setSq(cmd.sq);
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("sq=" + cmd.sq);
        response.end(); 
  } else if (pathname.search('/loadBtn')> -1) {
        //send back button settins for band
        
        var array = file.getBtnFreqs(pathname);
        //console.log('router: /loadBtn = ' + array);
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("loadBtn=," + array);
        response.end();        
  } else if (pathname.search('/setBtn=')> -1) {
        //post new button parmeters
        //parse out btn, band, freq, mode, filter key:value pairs

        cmd = querystring.parse(pathname.substring(pathname.lastIndexOf("/") + 1));
        
        //send to arrays & save files
        //console.log('Router: setBtn: ' + cmd.freq); 
        file.setMemArray(cmd.setBtn, cmd.band, cmd.freq, cmd.mode, cmd.filter);
        file.saveMemArrays();
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("setBtn=" + cmd.setBtn);
        response.end();
        
  } else if (pathname.search('/getBtn=')> -1) {
        //post new button parmeters
        //parse out btn, band, freq, mode, filter key:value pairs

        cmd = querystring.parse(pathname.substring(pathname.lastIndexOf("/") + 1));
        
        //send to arrays & save files
        var btnSettings = file.getMemArray(cmd.getBtn, cmd.band);
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write('getBtn' + cmd.getBtn + ',' + btnSettings);
        
        response.end();

      
  } else {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not found");
        response.end();  
  }}

exports.route = route;