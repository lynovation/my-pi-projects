var path = require('path');
var filePath = path.join(__dirname, + '') + '/';

var server = require(filePath + 'server');
var router= require(filePath + 'router');

var requestHandlers = require(filePath + 'requestHandlers');

var file = require(filePath + 'file');



var handle = {}; // ignore this error
handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;

handle["/upload"] = requestHandlers.upload;

file.loadMemArrays();

server.start(router.route, handle);