//Radio functions

var fs = require('fs');

var bb=require("/var/lib/cloud9/bonescript");

var serialPort=require("/home/root/node_modules/serialport/serialport").SerialPort;
var serial=new serialPort("/dev/ttyO1", { baudrate : 9600} );

exports.initRadio = function () {

    //setup uart1 pins 
    //echo 0 > /sys/kernel/debug/omap_mux/uart1_txd
    //echo 20 > /sys/kernel/debug/omap_mux/uart1_rxd
    try {
        var muxfileT = fs.openSync(
          "/sys/kernel/debug/omap_mux/uart1_txd", "w"
          );
        fs.writeSync(muxfileT, "0", null);
     } catch(ex2) {
        console.log("" + ex2);
        console.log("Unable to configure pinmux for uart1_txd");
    }

    try {
        var muxfileR = fs.openSync(
        "/sys/kernel/debug/omap_mux/uart1_rxd", "w"
        );
        fs.writeSync(muxfileR, "20", null);
    } catch(ex3) {
        console.log("" + ex3);
        console.log("Unable to configure pinmux for uart1_rxd");    
    }

    console.log("Init completed - initializing radio now...");

   //start radio
    txData("H101");
    txData("H101");
    txData("G301");
  
    txData("J5100"); //tone sq off
    txData("J5000"); //VSC off
    txData("J4380"); //IF Shift to mid range
    txData("J4500"); //AGC off
    txData("J4600"); //Noise blander off
    txData("J4700"); //Attenuator off
    
//console.log("Radio should be running now.");    

};

exports.radioOff = function () {
    //turn off radio
    txData("H100");
    txData("H100");
    //console.log("Radio has been turned off.");

};


exports.setFreq = function(freq, mode, filter) {
    //console.log("Setting Freq = " + freq + ", Mode = " + mode + ", Filter = " + filter);

    txData("K0" + freq + mode + filter + "00");
    //console.log('Sent to radio --> K0' + freq + mode + filter + '00'); 
    
};

exports.setVol = function(vol) {
    var volHex = Number(vol).toString(16);
    if (volHex.length < 2) {
        volHex = "0" + volHex;
    }
    //console.log("Setting Volume = 0x" + volHex); 
    txData("J40" + volHex);    
};

exports.setSq = function(sq) {
    var sqHex = Number(sq).toString(16);
    if (sqHex.length < 2) {
        sqHex = "0" + sqHex;
    }
    //console.log("Setting Squelch = 0x" + sqHex); 
    txData("J41" + sqHex);    
};


exports.ping = function() {
    //console.log("..........keep alive.......");
    //write directly to serial, no delay
    serial.write("H101" + "\r\n");  
};


exports.formatFreq2Radio = function(freq) {
 //format freq to send to radio
    freq = parseFloat(freq); //clip off label
    freq = freq + ''; //convert back to string
    console.log('comm: ' + freq);
    var ptr = freq.indexOf('.');
    if (ptr > -1) {
        //split GHz/MHz and other at '.'
        var temp = freq.split('.'); 
        while (temp[0].length < 4) {
            temp[0] = "0" + temp[0]; 
        }
        while (temp[1].length < 6) {
            temp[1] = temp[1] + "0";
        }
        freq = temp[0]+temp[1];
        //console.log(freq);
        return freq;
    } else {
        while (freq.length < 10) {
            freq = "0" + freq;
        }
        return freq;
    }
};

//////////////////////////////////////////////

function txData(txt) {
    serial.write(txt.toUpperCase() + "\r\n");
    bb.delay(20);
    //console.log("Raw Data= " + txt.toUpperCase());
}



 
