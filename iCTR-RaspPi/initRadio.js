//add the initRadio(); call to run this by its self 


var bb=require("/var/lib/cloud9/bonescript");

var fs = require("fs");

var serialPort=require("/home/root/node_modules/serialport/serialport").SerialPort;
var serial = new serialPort("/dev/ttyO1", { baudrate : 9600} );


function initRadio() {

//setup uart1 pins 
//echo 0 > /sys/kernel/debug/omap_mux/uart1_txd
//echo 20 > /sys/kernel/debug/omap_mux/uart1_rxd
try {
    var muxfileT = fs.openSync(
      "/sys/kernel/debug/omap_mux/uart1_txd", "w"
      );
    fs.writeSync(muxfileT, "0", null);
 } catch(ex2) {
    console.log("" + ex2);
    console.log("Unable to configure pinmux for uart1_txd");
}

try {
    var muxfileR = fs.openSync(
      "/sys/kernel/debug/omap_mux/uart1_rxd", "w"
      );
    fs.writeSync(muxfileR, "20", null);
 } catch(ex3) {
    console.log("" + ex3);
    console.log("Unable to configure pinmux for uart1_rxd");
}

console.log("Init completed - initializing radio now...");



   //start radio
    txData("H101");
    txData("G301");
    txData("K00094100000060400");
    txData("J4042");
    txData("J414D");
    
console.log("Radio should be running now.");    

}

function txData(txt)
{
    serial.write(txt + "\r\n");
    bb.delay(20);
}
 