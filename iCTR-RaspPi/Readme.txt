UPDATING IMAGE
To update the BeagleBone's Linux image, do the following

  1) Download new image file from BeagleBoard.org
  2) Unzip it using 7-Zip
  3) Reformat the SD card using SDFormatter
  4) Copy the new image file to the SD card using Win32DiskImager
  5) Install the Python compiler and the serial port driver using the instructions below.

LOADING SERIAL PORT DRIVERS
If you create a new image, or install iCTR from scratch, you must install the serial port drivers for Linux (not supplied in the default build).

To do this... 

  1) PuTTY into the BeagleBone and log into 'root' with no password 
  2) Run 'npm install python_compiler' to install the python compiler left out of Rev A6
  3) Run 'npm install serialport' to install the serial port drivers. You may have to do this a couple of times to get it to take.
  4) Copy the 'iCTR Beagle' folder from the PC to the /root/var/lib/cloud9/. Rename it to 'iCTR' on the BeagleBone.


USING GSTREAMER TO STREAM AUDIO
  BeagleBone Angstrum distribution includes gStreamer that interfaces with the Alsa environment. 
  1) Go to http://processors.wiki.ti.com/index.php/Example_GStreamer_Pipelines#OMAP35x for examples on how to use this
  2) Do a 'opkg install gst-plugins-good' command in root to download the latest gStreamer elements
  

NOTES ON THE HTML FILES


The index.html file for iCTR is created in Web Studio 5.0. WS5.0 does not allow all of the Javascripts needed for iCTR to be placed properly in the HTML file.

To fix these issues, you must manually do the following:

  1) Move the Javascript ta the top of the index.html file down to the bottom of the file. Insert it between the </body> and </html> tags.

  2) On the 'txtFreq' "div ID" entry, remove the Style description from WS5.0 and move the ">" down to the end of the Style description that follows. 
     The new file description sets the font, size and color of the txtFreq box.

  3) WS5.0 doesn't set the font size correctly. On memButX buttons, build font is 15, but font size in html is -20pt. Change this to 15pt.


NOTE: If IP service stops, restart the network by executing '/etc/init.d/networking restart'. Use 'netstat -nr' to check if IP address is assigned.

No-IP Dynamic DNS redirector

  User Name: lynovation56
  PW: Email w/@
  Setup host "iCTR.serverhttp.com" to point to my IP address - port 4256
  Installed IP Dynamic Update Client on Beaglebone
  To start DDNS, execute '/usr/local/bin/noip2' in root
